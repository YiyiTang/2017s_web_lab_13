package ictgradschool.web.lab13.ex03;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageGalleryDisplay extends HttpServlet {

    protected final String ending = "_thumbnail.png";
    protected final String basedir = "PhotoDir";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();
        //out.println("Hello");

        ServletContext ctx = getServletContext();

        String dir = ctx.getRealPath(ctx.getInitParameter(basedir));
        out.println("<html>\n<head><title>Server response</title>");
        out.println("</head>\n<body>");
        out.println("<h1>List of Images</h1>");

        File[] files = new File(dir).listFiles();

        for (File file : files) {
            String name = file.getName();

            if (name.endsWith(ending)) {
                out.println(makeItem(file));
            }
        }


        out.println("</body></html>");

    }

    private String makeItem(File file) {
        String name = file.getName();

        int idx = name.lastIndexOf(ending);
        String photoName = name.substring(0, idx);
        String linkPath = getServletContext().getInitParameter(basedir) + "/" + photoName + ".jpg";
        String thumbnailPath =  getServletContext().getInitParameter(basedir) + "/" + name;
        String text = "<p><a href=\"" + linkPath + "\" ><img src=\"" + thumbnailPath + "\" alt=\"thumb\"></a> " + photoName + " " + file.length() + "</p>";
        return text;
    }
}
