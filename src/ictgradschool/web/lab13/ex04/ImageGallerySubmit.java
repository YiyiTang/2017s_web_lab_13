package ictgradschool.web.lab13.ex04;


import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

public class ImageGallerySubmit  extends HttpServlet{


    private String photoDir;
    private int maxFileSize = 50 * 1024;
    private int maxMemSize = 4 * 1024;
    private File file ;

    @Override
    public void init() throws ServletException {
        photoDir = getServletContext().getRealPath(getServletContext().getInitParameter("PhotoDir"));
        System.out.println("PhotoDir:" + photoDir);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/excercise04.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        boolean isMultipart = ServletFileUpload.isMultipartContent(req);
        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();

        if ( !isMultipart) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p>No file uploaded</p>");
            out.println("</body>");
            out.println("</html>");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

        try {
            List<FileItem> fileItems  = upload.parseRequest(req);

            Iterator iter = fileItems.iterator();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");

            while (iter.hasNext()) {

                FileItem fi = (FileItem)iter.next();
                if ( !fi.isFormField () ) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String fileName = fi.getName();
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    if( fileName.lastIndexOf("\\") >= 0 ) {
                        file = new File( photoDir + fileName.substring( fileName.lastIndexOf("\\"))) ;
                    } else {
                        file = new File( photoDir + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
                    }
                    fi.write( file ) ;
                    out.println("Uploaded Filename: " + fileName + "<br>");

                    String thumbnailName = file.getName().substring(0, file.getName().lastIndexOf('.')) + "_thumbnail.png";
                    createThumbnail(file, photoDir + thumbnailName, 100, 100);

                }
            }

            out.println("</body>");
            out.println("</html>");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void createThumbnail(File file, String outputfile, int width, int height) {
        System.out.println("createThumbnail " + outputfile);
        BufferedImage before = null;
        try {
            before = ImageIO.read(file);
            int w = before.getWidth();
            int h = before.getHeight();

            Image toolkitImage = before.getScaledInstance(width, height,
                    Image.SCALE_SMOOTH);

            // width and height are of the toolkit image
            BufferedImage after = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_ARGB);
            Graphics g = after.getGraphics();
            g.drawImage(toolkitImage, 0, 0, null);
            g.dispose();

            File outfile = new File(outputfile);
            ImageIO.write(after, "png", outfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
